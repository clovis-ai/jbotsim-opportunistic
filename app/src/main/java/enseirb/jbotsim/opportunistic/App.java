package enseirb.jbotsim.opportunistic;

import io.jbotsim.core.Node;
import io.jbotsim.core.Topology;
import io.jbotsim.ui.JViewer;

import java.util.Arrays;
import java.util.Random;
import java.util.stream.Collectors;

public class App {

	static int numberOfMessages = 0;
	static int clockIterations = 0;

	static Random random;

	static void spawnNodes(int numNodes, int numInfrastructures, int numCircular, long seed, Topology tp) {

		int min_val_x = 100;
		int min_val_y = 100;
		int max_val_x = tp.getWidth() - 100;
		int max_val_y = tp.getHeight() - 100;

		boolean hasSource = false;
		boolean hasDestination = false;

		random = new Random(seed);

		for (int i = 0; i < numNodes; i++) {
			int locX = min_val_x + Math.abs(random.nextInt() % (max_val_x + 1));
			int locY = min_val_y + Math.abs(random.nextInt() % (max_val_y + 1));
			Node node = tp.newInstanceOfModel(Topology.DEFAULT_NODE_MODEL_NAME);
			if (!hasSource) {
				((AbstractNode) node).defineAsInfectedNode();
				hasSource = true;
				System.out.println(i + "\tSource selected");
			} else if (!hasDestination) {
				((AbstractNode) node).defineAsDestinationNode();
				hasDestination = true;
				System.out.println(i + "\tDestination selected");
			} else if (numInfrastructures > 0) {
				((AbstractNode) node).setInfrastructure();
				numInfrastructures--;
				System.out.println(i + "\tSet as infrastructure");
			} else if (numCircular > 0) {
				((AbstractNode) node).setCircularMovement();
				((AbstractNode) node).setRotationRadius(random.nextInt(100) + 1);
				numCircular--;
				System.out.println(i + "\tSet as circular");
			} else {
				System.out.println(i + "\tSet as linear");
			}
			tp.addNode(locX, locY, node);
		}
	}

	public static void main(String[] args) {
		Runtime.getRuntime().addShutdownHook(new Thread(App::printStatistics));
		new Thread(() -> {
			try {
				Thread.sleep(120_000L);
				System.out.println("*** TIMEOUT. ***");
				System.exit(5);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}).start();

		System.out.println("args\t" + Arrays.toString(args));

		int numCircular = 10;
		int numLinear = 10;
		int numInfrastructures = 10;
		long seed = 80000;

		Class<? extends Node> algorithm = EpidemicBroadcastNode.class;

		if (args.length > 0) {
			switch (args[0]) {
				case "custodian":
					algorithm = CustodyNode.class;
					break;
				case "epidemic":
					algorithm = EpidemicBroadcastNode.class;
					break;
				case "mobile":
					algorithm = MobileBroadcastNode.class;
					break;
				case "spray":
					algorithm = SprayAndWaitNode.class;
					break;
			}
		}
		System.out.println("algorithm\t" + algorithm);

		if (args.length > 1) {
			seed = Integer.parseInt(args[1]);
		}

		if (args.length > 4) {
			numInfrastructures = Integer.parseInt(args[2]);
			numCircular = Integer.parseInt(args[3]);
			numLinear = Integer.parseInt(args[4]);
		}

		System.out.println("seed\t" + seed);
		System.out.println("infra\t" + numInfrastructures);
		System.out.println("linear\t" + numLinear);
		System.out.println("circular\t" + numCircular);

		// Récupérer les dimensions de l'écran
		double width = 1000;
		double height = 500;

		Topology tp = new Topology((int) width, (int) height);
		tp.setDefaultNodeModel(algorithm);
		spawnNodes(numInfrastructures + numCircular + numLinear + 2, numInfrastructures,
		           numCircular,
		           seed, tp);

		new JViewer(tp);
		tp.start();
		registerHandlers(tp);

		var nodes = tp.getNodes().stream()
				.collect(Collectors.groupingBy(Node::getClass));

		System.out.println(nodes);
	}

	static void registerHandlers(Topology tp) {
		tp.addMessageListener(msg -> numberOfMessages++);
		tp.addClockListener(() -> clockIterations++);
		tp.addClockListener(() -> {
			Arrays.stream(AbstractNode.State.values())
					.forEach(state -> {
						var count = tp.getNodes().stream()
								.filter(node -> node instanceof AbstractNode)
								.filter(node -> ((AbstractNode) node).state == state)
								.count();

						assert !(count == 0 && state == AbstractNode.State.NONE);

						System.out.println(
								"clock\t" + clockIterations + "\t" + count + "\t" + state);
					});
		});

		tp.addClockListener(() -> {
			final var nodesThatHaventFinished = tp.getNodes()
					.stream()
					.filter(node -> ((AbstractNode) node).state != AbstractNode.State.END_PROG)
					.count();

			if (nodesThatHaventFinished == 0)
				System.exit(0);
		});
	}

	static void printStatistics() {
		System.out.println("\n");
		System.out.println("*** STATISTICS ***");
		System.out.println("messages\t" + numberOfMessages);
		System.out.println("iterations\t" + clockIterations);
	}
}
