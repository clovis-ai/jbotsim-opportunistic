package enseirb.jbotsim.opportunistic;

import io.jbotsim.core.Node;

public class Utils {

	private Utils() {
	}

	static void bounceOnWalls(Node node) {
		var incoming = node.getDirection();

		if (node.getLocation().x >= node.getTopology().getWidth() || node.getLocation().x < 0) {
			incoming -= Math.PI / 2;
		}
		if (node.getLocation().y >= node.getTopology().getHeight() || node.getLocation().y < 0) {
			incoming += Math.PI / 2;
		}

		node.setDirection(incoming);
	}

}
