package enseirb.jbotsim.opportunistic;

import io.jbotsim.core.Message;

import static enseirb.jbotsim.opportunistic.EpidemicBroadcastNode.MessageContent.*;


public class EpidemicBroadcastNode extends AbstractNode {

	@Override
	public void onClock() {
		super.onClock();

		if (state == State.INFECTED) {
			sendAll(new Message(VIRUS));
		}
		else if (state == State.CURED) {
			sendAll(new Message(CURE));
		}
		else if (state == State.END_PROG) {
			sendAll(new Message(END));
		}
	}

	@Override
	public void onSelection() {
		super.onSelection();

		if (!senderSelected) {
			setState(State.INFECTED);
			senderSelected = true;
		} else {
			setState(State.RECEIVER);
		}
	}

	@Override
	public void onMessage(Message message) {
		super.onMessage(message);

		MessageContent mc = (MessageContent) message.getContent();

		if (mc == END && state != State.END_PROG) {
			setState(State.END_PROG);
			return;
		}

		switch (state) {
			case RECEIVER:
				if (mc == VIRUS) {
					setState(State.CURED);
				}
				break;
			case INFECTED:
				if (mc == CURE) {
					if (isZeroCase) {
						setState(State.END_PROG);
						ENDED_NODES++;
					}
					else {
						setState(State.CURED);
					}
				}
				break;
			case NONE:
				if (mc == VIRUS) {
					setState(State.INFECTED);
				}
				if (mc == CURE) {
					setState(State.CURED);
				}
				break;
		}
	}

	static boolean senderSelected = false;

	enum MessageContent {
		VIRUS,
		CURE,
		END
	}
}
