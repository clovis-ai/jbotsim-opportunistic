package enseirb.jbotsim.opportunistic;

import io.jbotsim.core.Color;
import io.jbotsim.core.Link;
import io.jbotsim.core.Node;
import io.jbotsim.core.Point;

import java.util.ArrayList;
import java.util.List;

import static enseirb.jbotsim.opportunistic.Utils.bounceOnWalls;

public abstract class AbstractNode extends Node {

	@SuppressWarnings("FieldCanBeLocal")
	protected boolean isInfrastructure;
	protected boolean isZeroCase = false;
	private final double speed;

	protected State state = State.NONE;

	private boolean circularMovement = false;
	private double rotationRadius = 50;
	private Point rotationCenter;

	public AbstractNode() {
		this.speed = App.random.nextFloat();
	}

	@Override
	public void onStart() {
		super.onStart();

		if (isInfrastructure) {
			setColor(Color.GRAY);

			infrastructureNodes.stream()
					.filter(node -> node != this)
					.forEach(node -> getTopology().addLink(new Link(this, node)));
		} else {
			setDirection(Math.random() * Math.PI * 2);
			setColor(state.color);
		}

		if (circularMovement)
			rotationCenter = new Point(getX() + rotationRadius, getY() + rotationRadius);
	
	}

	@Override
	public void onClock() {
		super.onClock();

		if (!isInfrastructure)
			move(speed);
		bounceOnWalls(this);
	}

	public void rotate(double cx, double cy, double angle) {
		Point p = getLocation();

		p.setLocation(p.getX() -cx, p.getY() -cy);

		double xnew = p.getX() * Math.cos(angle) - p.getY() * Math.sin(angle);
		double ynew = p.getX() * Math.sin(angle) + p.getY() * Math.cos(angle);

		p.setLocation(xnew + cx, ynew + cy);

		setLocation(p);
	}
	
	@Override
	public void move(double distance) {
		if (!circularMovement)
			super.move(distance);

		else {
			double theta = distance  / rotationRadius;
			rotate(rotationCenter.getX(), rotationCenter.getY(), theta);
		}
	}

	void setState(State state) {
		this.state = state;
		setColor(state.color);
		if (state == State.RECEIVED_DONE)
			System.exit(0);
	}

	State getState() {
		return state;
	}

	public void defineAsInfectedNode() {
		setState(State.INFECTED);
		setColor(Color.red);
		isZeroCase = true;
	}

	public void defineAsDestinationNode() {
		setState(State.RECEIVER);
	}

	public void setInfrastructure() {
		isInfrastructure = true;
		infrastructureNodes.add(this);
	}

	public void setCircularMovement() {
		this.circularMovement = true;
	}

	public void setRotationRadius(double rotationRadius) {
		this.rotationRadius = rotationRadius;
	}

	static private final List<AbstractNode> infrastructureNodes = new ArrayList<>();
	static int ENDED_NODES = 0;

	enum State {
		RECEIVER(Color.blue),
		RECEIVED_DONE(Color.green),

		INFECTED(Color.red),
		CURED(Color.green),

		END_PROG(Color.black),

		NONE(null);

		Color color;

		State(Color color) {
			this.color = color;
		}
	}

	@Override
	public String toString() {
		return getID() + " " + state + (isInfrastructure ? " INFRA" : "");
	}
}
