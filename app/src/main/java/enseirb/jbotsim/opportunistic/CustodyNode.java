package enseirb.jbotsim.opportunistic;

import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

import java.util.Comparator;

public class CustodyNode extends AbstractNode {

	@Override
	public void onStart() {
		super.onStart();

		if (state == State.RECEIVER) {
			target = this;
		}
	}

	@Override
	public void onSelection() {
		if (!senderSelected) {
			setState(State.INFECTED);
			senderSelected = true;
		} else {
			setState(State.RECEIVER);
			target = this;
		}
	}

	@Override
	public void onClock() {
		super.onClock();
		if (state == State.INFECTED)
			getOutNeighbors().stream()
					.min(Comparator.comparing(target::distance))
					.ifPresent(node -> {
						if (target.distance(node) < target.distance(this)) {
							System.out.println("The carrier is now " + node.getID());
							send(node, new Message());
							setState(State.NONE);
						}
					});
	}

	@Override
	public void onMessage(Message message) {
		switch (state) {
			case NONE:
				setState(State.INFECTED);
				break;
			case RECEIVER:
				setState(State.RECEIVED_DONE);
				break;
		}
	}

	static boolean senderSelected = false;
	static CustodyNode target;

	static class NodeDistance {

		Node node;
		double distance;

		public NodeDistance(Node node, double distance) {
			this.node = node;
			this.distance = distance;
		}
	}
}
