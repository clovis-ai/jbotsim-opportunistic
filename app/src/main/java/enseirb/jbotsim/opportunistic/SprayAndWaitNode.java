package enseirb.jbotsim.opportunistic;


import io.jbotsim.core.Message;
import io.jbotsim.core.Node;

import java.util.List;

public class SprayAndWaitNode extends AbstractNode {

	private int numTokens = 0;

	@Override
	public void onStart() {
		super.onStart();

		if (state == State.INFECTED) {
			numTokens = NUM_INIT_TOKENS;
		}
	}

	@Override
	public void onClock() {
		super.onClock();

		if (state != State.NONE && state != State.RECEIVER) {
			List<Node> nodes = getNeighbors();
			for (Node node : nodes) {
				if (state == State.INFECTED) {
					if (isInfrastructure && ((AbstractNode) node).isInfrastructure && ((AbstractNode) node).getState() == State.NONE) {
						send(node, new Message(new TokenMessage(0, MessageContent.VIRUS)));
					} else if (isInfrastructure && !((AbstractNode) node).isInfrastructure && NUM_INFRA_TOKENS > 1 && ((AbstractNode) node).getState() == State.NONE) {
						send(node, new Message(new TokenMessage(NUM_INFRA_TOKENS / 2, MessageContent.VIRUS)));
						System.out.println("send Message");
						NUM_INFRA_TOKENS = NUM_INFRA_TOKENS - NUM_INFRA_TOKENS / 2;
					} else if ((((AbstractNode) node).getState() == State.NONE && numTokens > 1) || ((AbstractNode) node).getState() == State.RECEIVER) {
						send(node, new Message(new TokenMessage(numTokens / 2, MessageContent.VIRUS)));
						numTokens = numTokens - numTokens / 2;
					}
				} else if (state == State.CURED) {
					if (((AbstractNode) node).getState() != State.CURED)
						send(node, new Message(new TokenMessage(0, MessageContent.CURE)));
				} else if (state == State.END_PROG) {
					if (((AbstractNode) node).getState() != State.END_PROG) {
						send(node, new Message(new TokenMessage(0, MessageContent.END)));
					}
				}
			}
		}
	}
	@Override
	public void onSelection() {
		super.onSelection();

		if (!senderSelected) {
			setState(State.INFECTED);
			senderSelected = true;
		} else {
			setState(State.RECEIVER);
		}
	}

	@Override
	public void onMessage(Message message) {
		super.onMessage(message);

		TokenMessage tc = (TokenMessage) message.getContent();
		MessageContent mc = tc.getMessageContent();

		if (mc == MessageContent.END) {
			setState(State.END_PROG);
			return;
		}

		switch (state) {
			case RECEIVER:
				if (mc == MessageContent.VIRUS) {
					setState(State.CURED);
				}
				break;
			case INFECTED:
				if (mc == MessageContent.CURE) {
					if (isZeroCase) {
						setState(State.END_PROG);
						ENDED_NODES++;
					}
					else
						setState(State.CURED);
				}
				break;
			case NONE:
				if (mc == MessageContent.VIRUS) {
					setState(State.INFECTED);
					if (isInfrastructure && tc.getTokens() > 0) {
						NUM_INFRA_TOKENS = tc.getTokens();
						System.out.println("numInfraTokens : " + NUM_INFRA_TOKENS);
					}
					else
						numTokens = tc.getTokens();
				}
				if (mc == MessageContent.CURE) {
					setState(State.CURED);
				}
				break;
		}
	}

	static int NUM_INIT_TOKENS = 8;
	static boolean senderSelected = false;
	static int NUM_INFRA_TOKENS = 0;

	enum MessageContent {
		VIRUS,
		CURE,
		END
	}

	static class TokenMessage {
		private final int tokens;
		private final MessageContent mc;

		public TokenMessage (int tokens, MessageContent mc) {
			this.tokens = tokens;
			this.mc = mc;
		}

		public int getTokens() {
			return tokens;
		}

		public MessageContent getMessageContent() {
			return mc;
		}
	}
}
