# jbotsim-opportunistic

Opportunistic algorithms implemented with JBotSim

## How to use

To run the project, use the command:

```shell
./gradlew run <arguments>
```

The different available arguments are:

- `-Palgorithm=<custodian|epidemic|spray>` Starts the selected algorithm
- `-Pseed=<any integer>` Selects the seed used for pseudo-random generation
- `-PnumInfrastructure=<any positive integer>` Selects the number of infrastructure nodes
- `-PnumCircular=<any positive integer>` Selects the number of circular nodes
- `-PnumLinear=<any positive integer>` Selects the number of linear nodes

All arguments are mandatory. The order is not important. The library will automatically generate two additional linear nodes (the sender and the receiver).
