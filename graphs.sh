#!/usr/bin/env bash

function run() {
	echo "$1 $2 $3 $4 $5"
	./gradlew run -Palgorithm=$1 -Pseed=$2 -PnumInfrastructure=$3 -PnumCircular=$4 -PnumLinear=$5 >graphs-kotlin/src/main/resources/run_$1_$2_$3_$4_$5_$(date +%s).txt
}

for algo in custodian epidemic spray; do
	for circular in {0..20}; do
		for linear in {0..20}; do
			for infra in {0..20}; do
				for seed in 1 10 100 1000 10000 8587 39050 12 3495 9035622; do
					run $algo $seed $infra $circular $linear &
				done

				jobs -p
				wait < <(jobs -p)
			done
		done
	done
done
