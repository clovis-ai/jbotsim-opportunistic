import java.io.File
import java.util.*

data class Data(
	val algorithm: Algorithm,
	val seed: Int,
	val numberOfNodes: Map<NodeType, Int>,

	val histogram: List<Map<NodeState, Int>?>,

	val totalNumberOfMessages: Int,
	val totalNumberOfIterations: Int,

	val timeout: Boolean
)

enum class Algorithm(val className: String) {
	CUSTODIAN("enseirb.jbotsim.opportunistic.CustodyNode"),
	SPRAY_AND_WAIT("enseirb.jbotsim.opportunistic.EpidemicBroadcastNode"),
	EPIDEMIC("enseirb.jbotsim.opportunistic.SprayAndWaitNode");

	companion object {
		fun parse(line: String) = values().find { it.className in line }
	}
}

enum class NodeType(val shortHand: String) {
	LINEAR("infra"),
	INFRASTRUCTURE("linear"),
	CIRCULAR("circular");

	companion object {
		fun parseFromShorthand(text: String) = values().find { it.shortHand in text }
	}
}

enum class NodeState(val color: String) {
	RECEIVER("blue"),
	RECEIVED_DONE("green"),
	INFECTED("red"),
	CURED("green"),
	END_PROG("black"),
	NONE("pink");
}

fun parse(file: File): Data? {
	require(file.exists()) { "The file doesn't exist: $file" }

	var algorithm: Algorithm? = null
	var seed: Int? = null
	val numberOfNodes = HashMap<NodeType, Int>()
	var messages: Int? = null
	var iterations: Int? = null
	var timeout = false
	val histogram = HashMap<Int, MutableMap<NodeState, Int>>()

	file.bufferedReader().forEachLine { line ->
		when {
			line.startsWith("algorithm") -> algorithm = Algorithm.parse(line)
			line.startsWith("seed") -> seed = line.split("\t")[1].toInt()
			line.startsWith("messages") -> messages = line.split("\t")[1].toInt()
			line.startsWith("iterations") -> iterations = line.split("\t")[1].toInt()
			"TIMEOUT" in line -> timeout = true

			line.startsWith("clock") -> {
				val (_, time, number, state) = line.split("\t")
				val timeInt = time.toInt()
				val (stateE, numberInt) = (NodeState.values().find { it.name == state }
					?: error("Could not find node state $state")) to number.toInt()
				val map = histogram[timeInt] ?: EnumMap()
				map[stateE] = numberInt
				histogram[timeInt] = map
			}

			else -> {
				val typeShorthand = NodeType.values().find { line.startsWith(it.shortHand) }
				if (typeShorthand != null)
					numberOfNodes[typeShorthand] = line.split("\t")[1].toInt()
			}
		}
	}

	return Data(
		algorithm ?: return null.also { println("No algorithm in $file") },
		seed ?: return null.also { println("No seed in $file") },
		numberOfNodes,
		Array(histogram.size) { histogram[it + 1] }.toList(),
		messages ?: return null.also { println("No number of messages in $file") },
		iterations ?: return null.also { println("No number of iterations in $file") },
		timeout,
	)
}

inline fun <reified K : Enum<K>, V> EnumMap() = EnumMap<K, V>(K::class.java)
