import jetbrains.datalore.base.registration.Disposable
import jetbrains.datalore.plot.MonolithicCommon
import jetbrains.datalore.vis.swing.batik.DefaultPlotPanelBatik
import jetbrains.letsPlot.geom.geomSmooth
import jetbrains.letsPlot.intern.Plot
import jetbrains.letsPlot.intern.toSpec
import jetbrains.letsPlot.letsPlot
import java.awt.Dimension
import java.awt.GridLayout
import java.io.File
import java.util.stream.Stream
import javax.swing.*
import javax.swing.WindowConstants.EXIT_ON_CLOSE
import kotlin.streams.toList

fun <K> Map<K, Number>.averages(): Map<K, Double> {
	val input = this

	val totals = HashMap<K, Double>()

	input.forEach { (key, number) -> totals[key] = (totals[key] ?: 0.0) + number.toDouble() }

	return totals.mapValues { (key, total) -> total / input[key]!!.toDouble() }
}

fun List<Data>.perAlgo() = asSequence()
	.groupBy { it.algorithm }

fun Sequence<Data>.averageHistogram(): Map<Int, Map<NodeState, Double>> {
	val histogram = HashMap<Int, MutableMap<NodeState, Int>>()

	forEach { data ->
		val currentHistogram = data.histogram

		currentHistogram
			.filterNotNull()
			.forEachIndexed { time, states ->
				states.forEach { (state, count) ->
					histogram[time] =
						(histogram[time] ?: EnumMap<NodeState, Int>())
							.apply { put(state, (get(state) ?: 0) + count) }
				}
			}
	}

	return histogram.mapValues { it.value.averages() }
}

fun main() {

	val files = File("graphs-kotlin/src/main/resources")
		.listFiles()!!
		.toList()

	val filesStream: Stream<File> = files.stream()
		.parallel()
		.filter { it != null }
		.filter { it.canRead() }
		.filter { it.isFile }

	val dataPoints: List<Data> = filesStream
		.map { parse(it) }
		.toList()
		.filterNotNull()

	println("\n *** STATISTICS ***")
	println("Finished parsing ${dataPoints.size} entries")

	val perAlgo = dataPoints.perAlgo()

	perAlgo
		.mapValues { it.value.size }
		.forEach { (algo, count) ->
			println("$algo: $count records")
		}

	perAlgo
		.mapValues { it.value.asSequence().map { if (it.timeout) 1 else 0 }.average() }
		.forEach { (algo, timeouts) ->
			println("$algo: percentage of timeouts: ${timeouts * 100} %")
		}

	val perAlgoNoTimeouts = perAlgo
		.mapValues { it.value.filter { !it.timeout } }

	perAlgoNoTimeouts
		.mapValues { it.value.asSequence().map { it.totalNumberOfMessages }.average() }
		.forEach { (algo, messages) ->
			println("$algo: average of $messages messages")
		}

	perAlgoNoTimeouts
		.mapValues { it.value.asSequence().map { it.totalNumberOfIterations }.average() }
		.forEach { (algo, iterations) ->
			println("$algo: average of $iterations iterations")
		}

	val plots = Algorithm.values()
		.map { it to perAlgoNoTimeouts[it]!!.asSequence().averageHistogram() }
		.map { (name, histogram) -> name.toString() to createPlot(histogram) }
		.toMap()

	val selectedPlotKey = plots.keys.first()
	val controller = Controller(
		plots,
		selectedPlotKey,
		false
	)

	//region GUI

	val window = JFrame("Example App")
	window.defaultCloseOperation = EXIT_ON_CLOSE
	window.contentPane.layout = BoxLayout(window.contentPane, BoxLayout.Y_AXIS)

	// Add controls
	val controlsPanel = Box.createHorizontalBox().apply {
		// Plot selector
		val plotButtonGroup = ButtonGroup()
		for (key in plots.keys) {
			plotButtonGroup.add(
				JRadioButton(key, key == selectedPlotKey).apply {
					addActionListener {
						controller.plotKey = this.text
					}
				}
			)
		}

		this.add(Box.createHorizontalBox().apply {
			border = BorderFactory.createTitledBorder("Plot")
			for (elem in plotButtonGroup.elements) {
				add(elem)
			}
		})

		// Preserve aspect ratio selector
		val aspectRadioButtonGroup = ButtonGroup()
		aspectRadioButtonGroup.add(JRadioButton("Original", false).apply {
			addActionListener {
				controller.preserveAspectRadio = true
			}
		})
		aspectRadioButtonGroup.add(JRadioButton("Fit container", true).apply {
			addActionListener {
				controller.preserveAspectRadio = false
			}
		})

		this.add(Box.createHorizontalBox().apply {
			border = BorderFactory.createTitledBorder("Aspect ratio")
			for (elem in aspectRadioButtonGroup.elements) {
				add(elem)
			}
		})
	}
	window.contentPane.add(controlsPanel)

	// Add plot panel
	val plotContainerPanel = JPanel(GridLayout())
	window.contentPane.add(plotContainerPanel)

	controller.plotContainerPanel = plotContainerPanel
	controller.rebuildPlotComponent()

	SwingUtilities.invokeLater {
		window.pack()
		window.size = Dimension(850, 400)
		window.setLocationRelativeTo(null)
		window.isVisible = true
	}

	//endregion
}

private fun createPlot(d: Map<Int, Map<NodeState, Double>>): Plot {
	val data = mutableMapOf<String, Any>(
		"time" to d.keys,
	)

	val histogramByState = HashMap<NodeState, MutableMap<Int, Double>>()

	d.forEach { (index, d2) ->
		d2.forEach { (state, count) ->
			val d3 = histogramByState[state] ?: HashMap<Int, Double>().also {
				histogramByState[state] = it
			}

			val d4 = d3[index] ?: 0.0
			d3[index] = d4 + count
		}
	}

	histogramByState.forEach { (state, d5) ->
		data[state.name] = d5.values.toList().map { if (it.isNaN()) 0 else it }
	}

	var plot = letsPlot(data)

	for (state in NodeState.values()) {
		plot += /*geomPoint(
			color = state.color,
			fill = state.color,
			alpha = .3,
			size = 2
		) {
			x = "time"
			y = state.name
		} +*/ geomSmooth(
			color = state.color,
			alpha = 0.3,
			size = 2
		) {
			x = "time"
			y = state.name
		}
	}
	return plot
}

private class Controller(
	val plots: Map<String, Plot>,
	initialPlotKey: String,
	initialPreserveAspectRadio: Boolean
) {
	var plotContainerPanel: JPanel? = null
	var plotKey: String = initialPlotKey
		set(value) {
			field = value
			rebuildPlotComponent()
		}
	var preserveAspectRadio: Boolean = initialPreserveAspectRadio
		set(value) {
			field = value
			rebuildPlotComponent()
		}

	fun rebuildPlotComponent() {
		plotContainerPanel?.let {
			val container = plotContainerPanel!!
			// cleanup
			for (component in container.components) {
				if (component is Disposable) {
					component.dispose()
				}
			}
			container.removeAll()

			// build
			container.add(createPlotPanel())
			container.revalidate()
		}
	}

	fun createPlotPanel(): JPanel {
		val rawSpec = plots[plotKey]!!.toSpec()
		val processedSpec = MonolithicCommon.processRawSpecs(rawSpec, frontendOnly = false)

		return DefaultPlotPanelBatik(
			processedSpec = processedSpec,
			preserveAspectRatio = preserveAspectRadio,
			preferredSizeFromPlot = false,
			repaintDelay = 10,
		) { messages ->
			for (message in messages) {
				println("[Example App] $message")
			}
		}
	}
}
