plugins {
	java
	kotlin("jvm") version "1.5.0"
}

version = "unspecified"

repositories {
	mavenCentral()
}

dependencies {
	implementation(kotlin("stdlib"))
	implementation("org.jetbrains.kotlinx:kotlinx-html-jvm:0.7.3")
	implementation("org.jetbrains.lets-plot:lets-plot-batik:2.0.1")
	api("org.jetbrains.lets-plot:lets-plot-common:2.0.1")
	api("org.jetbrains.lets-plot:lets-plot-kotlin-api:2.0.1")
}

tasks.named<Copy>("processResources") {
	exclude("**/*")
}
